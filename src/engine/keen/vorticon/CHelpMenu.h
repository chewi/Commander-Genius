/*
 * CHelpMenu.h
 *
 *  Created on: 22.05.2010
 *      Author: gerstrong
 */

#ifndef CHELPMENU_H_
#define CHELPMENU_H_

#include "engine/core/menu/GameMenu.h"

class CHelpMenu : public GameMenu
{
public:
	CHelpMenu(const GsControl::Style style);
};

#endif /* CHELPMENU_H_ */
